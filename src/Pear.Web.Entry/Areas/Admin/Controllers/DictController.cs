﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace Pear.Web.Entry.Areas.Admin.Controllers
{

    /// <summary>
    /// 字典-后台控制器
    /// </summary>
    [Area("Admin")]
    [Route("Admin/[controller]/[action]")]
    [AppAuthorize]
    public class DictController : Controller
    {
        /// <summary>
        /// 字典主视图
        /// </summary>
        /// <returns></returns>
        [SecurityDefine("admin.system.dict:view")]
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 增加字典类型视图
        /// </summary>
        /// <returns></returns>
        [SecurityDefine("admin.system.dict:add")]
        public IActionResult AddType()
        {
            return View();
        }

        /// <summary>
        /// 编辑字典类型视图
        /// </summary>
        /// <returns></returns>
        [SecurityDefine("admin.system.dict:edit")]
        public IActionResult EditType()
        {
            return View();
        }

        /// <summary>
        /// 增加字典数据视图
        /// </summary>
        /// <returns></returns>
        [SecurityDefine("admin.system.dict:add")]
        public IActionResult AddData()
        {
            return View();
        }

        /// <summary>
        /// 编辑字典数据视图
        /// </summary>
        /// <returns></returns>
        [SecurityDefine("admin.system.dict:edit")]
        public IActionResult EditData()
        {
            return View();
        }


    }
}
