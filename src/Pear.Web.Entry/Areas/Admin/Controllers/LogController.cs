﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace Pear.Web.Entry.Areas.Admin.Controllers
{

    /// <summary>
    /// 日志-后台控制器
    /// </summary>
    [Area("Admin")]
    [Route("Admin/[controller]/[action]")]
    [AppAuthorize]
    public class LogController : Controller
    {
        /// <summary>
        /// 系统日志主视图
        /// </summary>
        /// <returns></returns>
        [SecurityDefine("admin.system.log:view")]
        public IActionResult Index()
        {
            return View();
        }

 
    }
}
