# 🍄 序言

修改和优化 Pear Admin Furion 项目为MVC。

喜欢的可以收藏备用，最终目标会成一个完善的后台权限框架。 

# 🔗 地址

[Pear Admin Furion 预览地址](http://furion.washala.com/)

[Swagger 文档](http://furion.washala.com/api/index.html)

[原作地址](https://gitee.com/pear-admin/pear-admin-furion)

# 🎮 任务 

#### 我做了什么?

- [x] 修改项目为MVC+API混合模式
- [x] 修改API授权为jwt+cookie
- [x] 修改角色分配授权页面
- [x] 修改API的权限控制
- [x] 修改数据库为Sqlite
- [x] 增加MVC架构的登录及退出
- [x] 增加视图页面的权限控制
- [x] 增加授权页面管理


#### 后面准备做什么?

- [ ] 基础设施
- [ ] 资源管理
- [ ] 配置中心
- [ ] 系统监控
- [ ] 定时任务
- [ ] 行为日志


# ✈ 上手试试

项目使用了Sqlite,但未包含迁移文件及数据库,需要根据官方说明完成 `Code First` （[实体模型生成数据库](https://monksoul.gitee.io/furion/docs/dbcontext-code-first#92022-%E6%89%93%E5%BC%80-%E7%A8%8B%E5%BA%8F%E5%8C%85%E7%AE%A1%E7%90%86%E6%8E%A7%E5%88%B6%E5%8F%B0)）

实际上只需要按照文档中的两步即可


*  创建模型版本

```
Add-Migration v1.0.0
```


*  更新到数据库

```
Update-Database
```
 
# ❤ 其他

另外我正在完善所有页面的功能,欢迎大家提交问题和PR。

 


